from src.detect import Detect
from flask import Flask, render_template, Response
import io
import cv2 as cv
import threading
import requests

app = Flask(__name__)

outputFrame = None
lock = threading.Lock()

main = None
src = 'http://cctv.balitower.co.id/Cempaka-Putih-Timur-002-700185_3/tracks-v1/mono.m3u8'
cap = cv.VideoCapture(src)

# Initial Flask for showing video realtime to dashboard
@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')

def generate():
	# grab global references to the output frame and lock variables
	global outputFrame, lock

	# loop over frames from the output stream
	while True:
		# wait until the lock is acquired
		with lock:
			# check if the output frame is available, otherwise skip
			# the iteration of the loop
			if outputFrame is None:
				continue

			# encode the frame in JPEG format
			(flag, encodedImage) = cv.imencode(".jpg", outputFrame)

			# ensure the frame was successfully encoded
			if not flag:
				continue

		# yield the output frame in the byte format
		yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
			bytearray(encodedImage) + b'\r\n')

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(
        generate(),
        mimetype='multipart/x-mixed-replace; boundary=frame'
    )

def run_detector():
    
    global cap, outputFrame, lock

    main = Detect("data/obj.names","cfg/yolov3-5th.cfg","weights/yolov3-5th_25000.weights")

    while cv.waitKey(1) < 0:
        # get frame from the video
        hasFrame, frame = cap.read()
        frameHeight = frame.shape[0]
        frameWidth = frame.shape[1]

        # print((frameHeight // 2) - 176)

        # cv.line(frame, (0, frameHeight // 2), (frameWidth, frameHeight // 2), (0, 255, 0), 2)
        cv.line(frame, (0, (frameHeight // 2) - 174), (frameWidth, (frameHeight // 2) - 174), (0, 255, 255), 2)
        # cv.line(frame, (0, (frameHeight // 2) + 150), (frameWidth, (frameHeight // 2) + 150), (0, 255, 255), 2)

        # Stop the program if reached end of video
        if not hasFrame:
            print("Done processing !!!")
            print("Output file is stored as ", outputFile)
            cv.waitKey(3000)
            # Release device
            cap.release()
            conn.close()
            break

        # Create a 4D blob from a frame.
        blob = cv.dnn.blobFromImage(frame, 1/255, (main.inpWidth, main.inpHeight), [0,0,0], 1, crop=False)

        # Sets the input to the network
        main.net.setInput(blob)

        # Runs the forward pass to get output of the output layers
        outs = main.net.forward(main.getOutputsNames(main.net))

        # Remove the bounding boxes with low confidence
        main.postprocess(frame, outs)

        # Put efficiency information. The function getPerfProfile returns the overall time for inference(t) and the timings for each of the layers(in layersTimes)
        t, _ = main.net.getPerfProfile()
        label = 'Inference time: %.2f ms' % (t * 1000.0 / cv.getTickFrequency())
        cv.putText(frame, label, (0, 15), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

        # Write the frame with the detection boxes

        # vid_writer.write(frame.astype(np.uint8))

        with lock:
            outputFrame = frame.copy()
        # cv.imshow(main.winName, frame)


if __name__ == "__main__" :

    # # construct the argument parser and parse command line arguments
	# ap = argparse.ArgumentParser()
	# ap.add_argument("-i", "--ip", type=str, required=True,
	# 	help="ip address of the device")
	# ap.add_argument("-o", "--port", type=int, required=True,
	# 	help="ephemeral port number of the server (1024 to 65535)")
	# ap.add_argument("-f", "--frame-count", type=int, default=32,
	# 	help="# of frames used to construct the background model")
	# args = vars(ap.parse_args())

    # start a thread that will perform motion detection
    t = threading.Thread(target=run_detector)
    t.daemon = True
    t.start()

    # start the flask app
    app.run(host='0.0.0.0', debug=True, threaded=True, use_reloader=False)
	# app.run(host=0, port=args["port"], debug=True,
	# 	threaded=True, use_reloader=False)

cap.release()