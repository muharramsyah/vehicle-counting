from datetime import date, datetime

class SaveDB:
    def __init__(self, con):
        self.con = con
    
    def saveToDB(self, obj, source, longitude, latitude):
        
        cursor = self.con.cursor()

        date_now = datetime.now()

        QUERY = 'INSERT INTO vehicle (obj, date_in, src_cctv, longitude, latitude) VALUES(%s, %s, %s, %s, %s)'

        params = (obj, date_now, source, longitude, latitude)

        cursor.execute(QUERY, params)

        self.con.commit()

        cursor.close()


