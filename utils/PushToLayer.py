import requests
import json
import logging
import threading
import time

class pushArcgis:
    def __init__(self):
        self.url = "https://services9.arcgis.com/XxwqNAxDionlvDDf/arcgis/rest/services/CCTV_Cempaka_Putih/FeatureServer/0/updateFeatures?token=v4iOwcawO3JeP9kRmbj5Vk11K7KK_ZDn-Od3n8MFngMHuHDCJSg_IsI_uy0W-SgjZroaIQtDUGovuZCiMUjVQRo-LTDPoAGDow6j2lTX2SO5TK_mWcdDoYNMZPBnmB4U9Fr9U55a7YsgEbFT1VSIkx81T03WHRsF0MwJpP55DXQ-EoY53GJ8Ggo1wI4X7Iq6Dy7uuHByIYJHTx_hbk3ZUzYuCRxkdON_9kz54y_jnjJIDgGGBGREioWoR5hT1Adb"

    def push(self, objectId, vehicle, total_car=None, url=None):

        attributes = {"objectId" : objectId, "total_car": total_car, "url_cctv": url}

        if('car' in vehicle):
            attributes["mobil"] = vehicle['car']
        if('bus' in vehicle):
            attributes["bus"] = vehicle['bus']
        if('truck' in vehicle):
            attributes["truck"] = vehicle['truck']

        features = {
            "attributes" : attributes
        }
        data = {
            "f" : "pjson",
            "features" : json.dumps(features)
        }
        print(data)
        x =requests.post(self.url, data = data) 

        print(x.json())
