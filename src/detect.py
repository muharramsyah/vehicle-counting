import cv2 as cv
import numpy as np
import argparse
import sys
import os.path
import math
import requests
import time
import io
import threading 

from counting.centroidtracker import CentroidTracker
from counting.trackableobject import TrackableObject
from utils.PushToLayer import pushArcgis

class Detect:
    def __init__(self, path_obj_name, path_configuration_model, path_weight_model):

        self.ip_public_machine = requests.get('http://ip.42.pl/raw').text


        # initialize the output frame and a lock used to ensure thread-safe
        # exchanges of the output frames (useful for multiple browsers/tabs
        # are viewing tthe stream)

        # Initialize the parameters
        self.confThreshold = 0.6  #Confidence threshold
        self.nmsThreshold = 0.4   #Non-maximum suppression threshold
        self.inpWidth = 416       #Width of network's input image
        self.inpHeight = 416      #Height of network's input image

        # Load names of classes
        self.classesFile = path_obj_name;
        self.classes = None

        # Give the configuration and weight files for the model and load the network using them.
        self.modelConfiguration = path_configuration_model;
        self.modelWeights = path_weight_model;

        # initialize the video writer
        self.writer = None

        # initialize the frame dimensions (we'll set them as soon as we read
        # the first frame from the video)
        self.W = None
        self.H = None

        # initial Counting vehicle
        self.totalDown = 0
        self.totalUp = 0
        self.argis = pushArcgis()
        
        with open(self.classesFile, 'rt') as f:
            self.classes = f.read().rstrip('\n').split('\n')

        self.init_class = {cls : 0 for cls in self.classes}

        # Process inputs
        self.winName = 'Vehicle Classification and Tracking'
        # cv.namedWindow(self.winName, cv.WINDOW_NORMAL)


        # load our serialized model from disk
        print("[INFO] loading model...")
        self.net = cv.dnn.readNetFromDarknet(self.modelConfiguration, self.modelWeights)
        self.net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
        self.net.setPreferableTarget(cv.dnn.DNN_TARGET_OPENCL)

        # instantiate our centroid tracker, then initialize a list to store
        # each of our dlib correlation trackers, followed by a dictionary to
        # map each unique object ID to a TrackableObject
        self.ct = CentroidTracker(maxDisappeared=40, maxDistance=50)
        self.trackers = []
        self.trackableObjects = {}


    # def __del__(self):
    #     self.cap.release()

    # Get the names of the output layers
    def getOutputsNames(self, net):
        # Get the names of all the layers in the network
        layersNames = net.getLayerNames()
        # Get the names of the output layers, i.e. the layers with unconnected outputs
        return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    # Draw the predicted bounding box
    def drawPred(self, frame, classId, conf, left, top, right, bottom):
        # Draw a bounding box.
        cv.rectangle(frame, (left, top), (right, bottom), (255, 178, 50), 2)
        # Draw a center of a bounding box
        frameHeight = frame.shape[0]
        frameWidth = frame.shape[1]
        # cv.line(frame, (0, frameHeight//2 - 50), (frameWidth, frameHeight//2 - 50), (0, 255, 255), 2)
        cv.circle(frame,(left+(right-left)//2, top+(bottom-top)//2), 3, (0,0,255), -1)

        # Put text on upside of bounding box
        text = "{} : {}".format(self.classes[classId], conf)
        cv.putText(frame, text, (left, top - 10),
                cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        
        coun = 0
        counter = []
        if (top+(bottom-top)//2 in range(frameHeight//2 - 2,frameHeight//2 + 2)):
            coun +=1
            #print(coun)

            counter.append(coun)

        # label = 'Pedestrians: '.format(str(counter))
        # cv.putText(frame, label, (0, 30), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

    # Remove the bounding boxes with low confidence using non-maxima suppression
    def postprocess(self, frame, outs):
        frameHeight = frame.shape[0]
        frameWidth = frame.shape[1]

        rects = []

        # Scan through all the bounding boxes output from the network and keep only the
        # ones with high confidence scores. Assign the box's class label as the class with the highest score.
        classIds = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                classId = np.argmax(scores)
                confidence = scores[classId]
                if confidence > self.confThreshold:
                    center_x = int(detection[0] * frameWidth)
                    center_y = int(detection[1] * frameHeight)
                    width = int(detection[2] * frameWidth)
                    height = int(detection[3] * frameHeight)
                    left = int(center_x - width / 2)
                    top = int(center_y - height / 2)
                    classIds.append(classId)
                    confidences.append(float(confidence))
                    boxes.append([left, top, width, height])

        # Perform non maximum suppression to eliminate redundant overlapping boxes with
        # lower confidences.
        indices = cv.dnn.NMSBoxes(boxes, confidences, self.confThreshold, self.nmsThreshold)
        for i in indices:
            i = i[0]
            box = boxes[i]
            left = box[0]
            top = box[1]
            width = box[2]
            height = box[3]
            # Class "person"
            # if classIds[i] == 0:
            rects.append((left, top, left + width, top + height))
            # use the centroid tracker to associate the (1) old object
            # centroids with (2) the newly computed object centroids
            objects = self.ct.update(rects)
            self.counting(frame, classIds[i],objects)

            self.drawPred(frame, classIds[i], confidences[i], left, top, left + width, top + height)

    def counting(self, frame, cls_idx, objects):
        frameHeight = frame.shape[0]
        frameWidth = frame.shape[1]

        self.totalDown
        self.totalUp

        # loop over the tracked objects
        for (objectID, centroid) in objects.items():
            # check to see if a trackable object exists for the current
            # object ID
            to = self.trackableObjects.get(objectID, None)
    
            # if there is no existing trackable object, create one
            if to is None:
                to = TrackableObject(objectID, centroid)
    
            # otherwise, there is a trackable object so we can utilize it
            # to determine direction
            else:
                # the difference between the y-coordinate of the *current*
                # centroid and the mean of *previous* centroids will tell
                # us in which direction the object is moving (negative for
                # 'up' and positive for 'down')
                y = [c[1] for c in to.centroids]
                direction = centroid[1] - np.mean(y)
                to.centroids.append(centroid)
    
                # check to see if the object has been counted or not
                if not to.counted:
                    # if the direction is negative (indicating the object
                    # is moving up) AND the centroid is above the center
                    # line, count the object
                    if direction < 0 and centroid[1] in range(frameHeight//2 - 100, frameHeight//2 + 100):
                        self.totalUp += 1
                        to.counted = True
                        self.init_class[self.classes[cls_idx]] +=1
                        # self.argis.push(1, self.init_class, total_car = (self.totalUp+self.totalDown))
                        x = threading.Thread(target=self.argis.push(1, self.init_class, total_car = (self.totalUp+self.totalDown), url=self.ip_public_machine), args=(self.totalDown+self.totalUp+1,))
                        x.start()

                    # if the direction is positive (indicating the object
                    # is moving down) AND the centroid is below the
                    # center line, count the object
                    elif direction > 0 and centroid[1] in range(frameHeight//2 - 250, frameHeight//2 + 250):
                        self.totalDown += 1
                        to.counted = True
                        self.init_class[self.classes[cls_idx]] +=1
                        # self.argis.push(1, self.init_class, total_car = (self.totalUp+self.totalDown))
                        x = threading.Thread(target=self.argis.push(1, self.init_class, total_car = (self.totalUp+self.totalDown), url=self.ip_public_machine), args=(self.totalDown+self.totalUp+1,))
                        x.start()


                    print("Direction : {}, Centroid : {}".format(direction, centroid[1]))


            # store the trackable object in our dictionary
            self.trackableObjects[objectID] = to
            # draw both the ID of the object and the centroid of the
            # object on the output frame
            text = "{}-{}".format(self.classes[cls_idx],objectID)
            cv.putText(frame, text, (centroid[0] - 10, centroid[1] - 10),
                cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv.circle(frame, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)
        # construct a tuple of information we will be displaying on the
        # frame
        info = [
            ("Up", self.totalUp),
            ("Down", self.totalDown),
        ]

        print(self.init_class)
        # loop over the info tuples and draw them on our frame
        for (i, (k, v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            cv.putText(frame, text, (10, frameHeight - ((i * 20) + 20)),
                cv.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)

    
